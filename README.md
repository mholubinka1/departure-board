# UK Train Departure Display 

A set of python scripts to display replica near real-time UK railway station departure data on SSD13xx style screens. 
Uses the publicly available [Transport API](https://www.transportapi.com/).  

   * [Hardware](#hardware)
   * [Installation](#installation)
   * [Configuration](#configuration)
   * [Running](#running)
   * [ToDo](#ToDo)
   * [Thanks](#Thanks)

![](normal.gif)

## Hardware
This project is designed to run on any Raspberry Pi using the GPIO pins for output. 
By default 4SPI will be used (and is suggested for simplicity) however I2C and 3SPI may be used with various run options. Ensure you have a display that is set for 4SPI or can be modified to run in 4SPI mode.

<details>
<summary>Suggested Hardware</summary>

- [Raspberry Pi Zero W](https://thepihut.com/collections/raspberry-pi/products/raspberry-pi-zero-wh-with-pre-soldered-header)

- SD13XX display [eBay](https://www.ebay.co.uk/itm/New-Yellow-3-12-256x64-Pixel-OLED-LED-Display-Module-SSD1322/252552688521)

- [Micro SD Card](https://thepihut.com/collections/raspberry-pi-sd-cards-and-adapters/products/noobs-preinstalled-sd-card)

- [Jumper Wires](https://thepihut.com/products/rpi-premium-jumper-wires-40pk-female-female-100mm)

- Micro USB cable for power
</details>

<details>
<summary>Pinout</summary>

![](luma-oled-spi-pin-configuration.png "SPI Pinout")
</details>


## Installation

To run this code, you will need Python 3.6+

>### Raspbian Lite
>If you're using Raspbian Lite, you'll also need to install:
>- `libopenjp2-7`
>with:
>```bash
>$ sudo apt-get install libopenjp2-7
>```

- Clone this repo
- Install dependencies
- <details><summary>Enable the SPI interface</summary>

  Edit `/boot/config.txt` ensure the line `dtparam=spi=on` is not commented out.
  After a reboot, `/dev/spidev0.0` and/or `/dev/spidev0.1` should be visible
  </summary>

## Configuration 

Sign up for the [Transport API](https://www.transportapi.com/), and generate an app ID and API key (note the free tier has 1000 request a day).

Copy `config.sample.json` to `config.json` and complete.

```javascript
{
  "journey": {
    "departureStation": "",
    "destinationStation": null,
    "stationAbbr": {
      "International": "Intl."
    },
    "outOfHoursName": "Hogwarts"
  },
  "refreshTime": 180,
  "transportApi": {
    "appId": "",
    "apiKey": "",
    "operatingHours": "0-23"
  }
}
```

### General Settings

`refreshTime` - Fetch frequency for new data from the transport api. There will be two api calls each time this time elapses, be aware the free tier has 1000 calls a day.

### Journey Settings

`departureStation` - The [short code](https://www.nationalrail.co.uk/stations_destinations/48541.aspx) for the starting station. 

`destinationStation` - The optional [short code](https://www.nationalrail.co.uk/stations_destinations/48541.aspx) for the destination station. 

`stationAbbr` - A list of words and their abbreviations that can be used to shorten station names, useful for small displays. 

`outOfHoursName` - The station name to display when out of hours. 

### Transport API Settings

`appId` - Your transport API application ID

`apiKey` - Your transport API key

`operating hours` - The range of hours you wish to actively request train times. Be aware the free tier API has a limit of 1000 calls a day.


## Running

You can use the make run command to start the display with the default settings.
Otherwise you can edit the command below, and add options from the luma library to extend the script.

```bash
$ python ./src/main.py --display ssd1322 --width 256 --height 64 --interface spi --mode 1 --rotate 2 
```

## Example Output

### Normal Operating Hours
![](normal.gif)
### Out Of Hours / No Trains
![](outofhours.gif)


## ToDo

- Add case design for 3d printing
- Async requests for fetching
- Variable refresh rates


## Thanks

A big thanks to Chris Hutchinson who originally built this code! He can be found on GitHub at https://github.com/chrishutchinson/

The fonts used were painstakingly put together by `DanielHartUK` and can be found on GitHub at https://github.com/DanielHartUK/Dot-Matrix-Typeface - A huge thanks for making that resource available!

Huge shoutout to `ghostseven` who did a lot of work backporting changes from the Balena guide.